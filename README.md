**Small Disk Utilities**
(*formerly ISO-From-Folder*)

This is a GUI wrapper around some OS X / macOS Terminal commands to allow users to easily:

- create ISO files from the contents of a folder.
- create unformatted, empty floppy disk images for use in Virtualisation (eg VirtualBox, VMWare)

The resultant files are placed on your Desktop.

The source code is written in [PureBasic](https://www.purebasic.com), a modern implementation of the classic BASIC language. PureBasic however owes more than a passing nod to C, as it makes extensive use of structures, pointers, etc.

The source may be small enough to compile with the free version (I have not tried this however). 

**Note: Although PureBasic code is cross platform (Mac, Linux and Windows), this program uses Mac-specific features and so will not work if compiled on other platforms.**


You can download a pre-compiled binary from the the public files area of my website [shimeril.com](https://shimeril.com/free/)

Initial README: 11 July 2016

Last updated: 04 August 2021
