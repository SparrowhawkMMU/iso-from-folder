﻿; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}


; entry point into system - just includes required files and runs a minimalist event loop
; window and gadget events are handled in the window module(s)
EnableExplicit

XIncludeFile "Common.pbi"

XIncludeFile "DeclareWindowMain.pbi"
XIncludeFile "DeclareFileSystem.pbi"
XIncludeFile "DeclareDiskUtilities.pbi"

XIncludeFile "WindowMain.pbi"
XIncludeFile "FileSystem.pbi"
XIncludeFile "DiskUtilities.pbi"

Define eventNumber.i = 0

_WindowMain::Open()

Repeat
	
	eventNumber = WaitWindowEvent()	
	
	Select eventNumber
		Case _Common::#EVENT_Quit
			Break
			
		Case #PB_Event_Menu
			
			Select EventMenu()
				; Quit menu
				Case -4
					Break
					
			EndSelect
			
	EndSelect
	
ForEver

End
