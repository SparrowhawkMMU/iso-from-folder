﻿; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}


; Provides functions To simplify interactions With the filesystem

Module _FileSystem
	
	EnableExplicit
	
	; checks whether the supplied path resolves to a folder
	Procedure.b IsFolder(path.s)
		Debug ">> _FileSystem::IsFolder()"
		
		Protected isFolder.b = Bool(FileSize(path) = -2)			
		ProcedureReturn isFolder
	EndProcedure
	
	; checks whether the supplied file path exists and is not a directory
	Procedure.b FileExists(filePath.s)
		Debug ">> _FileSystem::FileExists()"
		
		Protected exists.b = Bool(FileSize(filePath) >= 0)			
		ProcedureReturn exists	
	EndProcedure
	
EndModule

	