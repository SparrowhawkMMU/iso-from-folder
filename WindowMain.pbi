﻿; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}


; the main window - offers user the available disk handling options 

Module _WindowMain
	
	EnableExplicit
	
	UseModule _Common
	
	Enumeration Windows
		#WINDOW_Main	
	EndEnumeration
	
	Enumeration Controls
		#CONTROL_UtilitiesPanel
		
		#CONTROL_SourceFolderString
		#CONTROL_SelectSourceFolderButton
		#CONTROL_CreateISOButton
		
		#CONTROL_CreateEmptyFloppyDiskButton
		
		#CONTROL_ProgessText
	EndEnumeration
	
	; module level (private) properties
	Global m_TestMode.b = #True
	
	; module-level (private) procedure declarations
	Declare Initialise()
	Declare FolderPathChanged()
	Declare SelectSourceFolderButtonClicked()
	Declare CreateISOButtonClicked()
	Declare CreateEmptyFloppyButtonClicked()
	
	
	;{ module-level procedure implementations
	
	; Open the window and set the gadgets
	Procedure Open()
		Debug ">>> _WindowMain:Open()"
		
		If OpenWindow(#WINDOW_Main, #PB_Ignore, #PB_Ignore, 600, 230, #SYSTEM_Name + " V" + #SYSTEM_Version, #PB_Window_SystemMenu | #PB_Window_ScreenCentered)
			
			;TODO use new XML dialog library to define window - allows resizable gadgets
			
			
			PanelGadget(#CONTROL_UtilitiesPanel, 10, 10, 580, 170)
			AddGadgetItem (#CONTROL_UtilitiesPanel, -1, "ISO from Folder")
      		
			TextGadget(#PB_Any, 20, 20, 100, 25, "Source Folder:")
			StringGadget(#CONTROL_SourceFolderString, 120, 20, 370, 25, "")
			ButtonGadget(#CONTROL_SelectSourceFolderButton, 500, 20, 40, 25, "...")
			
			ButtonGadget(#CONTROL_CreateISOButton, 440, 60, 100, 25, "Create ISO")
			
			AddGadgetItem (#CONTROL_UtilitiesPanel, -1, "Create Empty Floppy")
			TextGadget(#PB_Any, 20, 20, 320, 100, "This will create an empty, unformatted floppy disk image on your desktop, for use with, for example, VirtualBox, VMWare etc")
			ButtonGadget(#CONTROL_CreateEmptyFloppyDiskButton, 430, 60, 110, 25, "Create Floppy")
			CloseGadgetList()
			
			TextGadget(#CONTROL_ProgessText, 20, 190, 380, 25, "")
			
			BindGadgetEvent(#CONTROL_SelectSourceFolderButton, @SelectSourceFolderButtonClicked())
			BindGadgetEvent(#CONTROL_SourceFolderString, @FolderPathChanged(), #PB_EventType_Change)
			BindGadgetEvent(#CONTROL_CreateISOButton, @CreateISOButtonClicked())
			
			BindGadgetEvent(#CONTROL_CreateEmptyFloppyDiskButton, @CreateEmptyFloppyButtonClicked())
			
			BindEvent(#PB_Event_CloseWindow, @Close(), #WINDOW_Main)
			
			Initialise()
			
			; put a default value in when testing
			If m_TestMode
				SetGadgetText(#CONTROL_SourceFolderString, GetHomeDirectory() + "Documents")
				DisableGadget(#CONTROL_CreateISOButton, #False)
			EndIf
			
		Else
			Debug "Failed to open Window #" + Str(#WINDOW_Main)
		EndIf
		
	EndProcedure
	
	
	; Initialise the window's gadgets
	Procedure Initialise()
		Debug ">>> _WindowMain:Initialise()"
		
		SetGadgetText(#CONTROL_ProgessText, "")
		SetGadgetText(#CONTROL_SourceFolderString, "")
		DisableGadget(#CONTROL_CreateISOButton, #True)
		SetActiveGadget(#CONTROL_SourceFolderString)
		
	EndProcedure
	
	; user has clicked on button to open folder requester
	Procedure SelectSourceFolderButtonClicked()
		Debug ">>> _WindowMain:SelectSourceFolderButtonClicked()"
		
		Protected selectedFolder.s = PathRequester("Select source folder", GetHomeDirectory())
		
		If Not selectedFolder = ""
			SetGadgetText(#CONTROL_SourceFolderString, selectedFolder)
		EndIf
		
	EndProcedure
	
	
	; User has entered a path
	Procedure FolderPathChanged()
		Debug ">>> _WindowMain:FolderPathChanged()"
		
		Protected folderPath.s = Trim(GetGadgetText(#CONTROL_SourceFolderString))
		
		If folderPath = ""
			DisableGadget(#CONTROL_CreateISOButton, #True)
		Else
			If _FileSystem::IsFolder(folderPath)
				DisableGadget(#CONTROL_CreateISOButton, #False)
			Else
				DisableGadget(#CONTROL_CreateISOButton, #True)
			EndIf
		EndIf

	EndProcedure
	
	
	; "Create ISO" button clicked event handler
	Procedure CreateISOButtonClicked()
		Debug ">>> _WindowMain:CreateISOButtonClicked()"
		
		Protected sourceFolderPath.s = Trim(GetGadgetText(#CONTROL_SourceFolderString))
		Protected outputFolderPath.s = GetHomeDirectory() + "Desktop"
		Protected outputFileName.s   = "output.iso"
		
		DisableGadget(#CONTROL_CreateISOButton, #True)
		
		SetGadgetText(#CONTROL_ProgessText, "Checking...")
		If Not _FileSystem::IsFolder(sourceFolderPath)
			MessageRequester("Error", "Folder " + sourceFolderPath + " not found", #PB_MessageRequester_Warning)
			SetGadgetText(#CONTROL_ProgessText, "ISO creation was cancelled")
			Goto ExitProc
		EndIf
		
		If Not _FileSystem::IsFolder(outputFolderPath)
			MessageRequester("Error", "Folder " + outputFolderPath + " not found", #PB_MessageRequester_Warning)
			SetGadgetText(#CONTROL_ProgessText, "ISO creation was cancelled")
			Goto ExitProc
		EndIf
		
		If _FileSystem::FileExists(outputFolderPath + "/" + outputFileName)
			If MessageRequester("File Exists", "File already exists - overwrite it?", #PB_MessageRequester_YesNo | #PB_MessageRequester_Warning) = #PB_MessageRequester_No
				Goto ExitProc
			EndIf
		EndIf
		
		SetGadgetText(#CONTROL_ProgessText, "Starting...")
		
		If _DiskUtilities::CreateISO(sourceFolderPath, outputFolderPath)
			SetGadgetText(#CONTROL_ProgessText, "Finished... ISO saved to " + outputFolderPath + "/" + outputFileName)
		Else
			SetGadgetText(#CONTROL_ProgessText, "ERROR: Failed to create ISO")
		EndIf	
			
		ExitProc:
		DisableGadget(#CONTROL_CreateISOButton, #False)
		
	EndProcedure
	
	
		; "Create ISO" button clicked event handler
	Procedure CreateEmptyFloppyButtonClicked()
		Debug ">>> _WindowMain:CreateEmptyFloppyButtonClicked()"
		
		Protected outputFolderPath.s = GetHomeDirectory() + "Desktop"
		Protected outputFileName.s   = "Empty_Floppy.img"
		
		DisableGadget(#CONTROL_CreateEmptyFloppyDiskButton, #True)
		
		If _FileSystem::FileExists(outputFolderPath + "/" + outputFileName)
			If MessageRequester("File Exists", "File already exists - overwrite it?", #PB_MessageRequester_YesNo | #PB_MessageRequester_Warning) = #PB_MessageRequester_No
				Goto ExitProc
			EndIf
		EndIf
		
		SetGadgetText(#CONTROL_ProgessText, "Starting...")
		
		If _DiskUtilities::CreateEmptyFloppyImage(1440, 1024, outputFileName, outputFolderPath)
			SetGadgetText(#CONTROL_ProgessText, "Finished... Floppy image saved to " + outputFolderPath + "/" + outputFileName)
		Else
			SetGadgetText(#CONTROL_ProgessText, "ERROR: Failed to create floppy image")
		EndIf	
			
		ExitProc:
		DisableGadget(#CONTROL_CreateEmptyFloppyDiskButton, #False)
		
	EndProcedure
	
	
	
	; Close the window
	Procedure Close()
		Debug ">>> _WindowMain:Close()"
		PostEvent(#EVENT_Quit)
	EndProcedure

	;}
	
EndModule
